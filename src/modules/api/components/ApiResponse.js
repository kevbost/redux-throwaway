import React from 'react'
import ReactJson from 'react-json-view'

const ApiResponse = ( props ) => {

  const { isLoading, responseData } = props

  const SubmitText = () => {
    return (
      <p className="center">
        {'Submit an API Request to view a response.'}
      </p>
    )
  }

  const JsonLoading = () => {
    return (
      <div className="react-json-view">
        {'Fetching results …'}
      </div>
    )
  }

  const JsonDisplay = () => {
    return (
      <ReactJson
        src={responseData}
        name={false}
        displayObjectSize={false}
        displayDataTypes={false}
        enableClipboard={false}
        iconStyle={'circle'}
      />
    )
  }


  const display = () => {
    if ( isLoading ) {
      return <JsonLoading />
    } else if ( responseData && Object.keys( responseData ).length > 0 ) {
      return <JsonDisplay />
    } else {
      return <SubmitText />
    }
  }

  return (
    <div>
      {display()}
    </div>
  )
}

export default ApiResponse
