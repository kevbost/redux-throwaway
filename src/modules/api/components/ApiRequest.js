import React from 'react'
import { ButtonGroup, Button } from 'react-bootstrap'

const ApiRequest = ( props ) => {
  const RequestButtons = () => {
    const {send_request} = props

    const handleClick = ( e ) => {
      let presort = true
      try {
        e.target.attributes.presort.value
      } catch ( e ){
        presort = false
      }
      send_request( e.target.attributes.cty.value, e.target.attributes.timespan.value, presort )
    }

    return (
      <ButtonGroup>
        <Button
          onClick={handleClick}
          cty="gvl" timespan="tonight"
        >{'Gvl Tonight'}</Button>
        <Button
          onClick={handleClick}
          cty="gvl" timespan="thisweek"
        >{'Gvl This Week'}</Button>
        <Button
          onClick={handleClick}
          cty="avl" timespan="tonight"
        >{'Avl Tonight'}</Button>
        <Button
          onClick={handleClick}
          cty="avl" timespan="thisweek"
        >{'Avl This Week'}</Button>
        <Button
          onClick={handleClick}
          cty="gvl" timespan="thisweek"
          presort="true"
        >{'Gvl Presorted'}</Button>
        <Button
          onClick={handleClick}
          cty="avl" timespan="thisweek"
          presort="true"
        >{'Avl Presorted'}</Button>
      </ButtonGroup>
    )
  }

  return (
    <div>
      <RequestButtons />
    </div>
  )
}

export default ApiRequest
