import React from 'react'
import ApiResponse from './components/ApiResponse'
import ApiRequest from './components/ApiRequest'
import { captureStatus, getLoadingStatus } from 'redux-thunk-status'
import { submitRequest } from './actions'
import { connect } from 'react-redux'

class ApiContainer extends React.Component {

  render() {
    const { isLoading,
      responseData,
      send_request } = this.props

    return (
      <div>
        <ApiRequest
          send_request={send_request}
        />
        <br />
        <ApiResponse isLoading={isLoading} responseData={responseData} />
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    responseData: state.api.responseData,
    isLoading: getLoadingStatus( 'submit_request' )( state )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    send_request: ( cty, timespan, presort ) => {
      dispatch( captureStatus( 'submit_request', submitRequest( cty, timespan, presort ) ) )
    }
  }
}

export default connect( mapStateToProps, mapDispatchToProps )( ApiContainer )
