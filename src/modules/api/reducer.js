import * as types from './actionTypes'
import update from 'immutability-helper'

export const INITIAL_STATE = {
  gvl: {},
  avl: {}
}

export default ( state = INITIAL_STATE, action ) => {
  switch ( action.type ) {
  case types.SET_RESPONSE:
    return update( state, {
      [action.cty]: {
        [action.timespan]: {
          $set: action.data
        }
      },
      responseData: {
        $set: action.data
      }
    })

  default:
    return state
  }
}
