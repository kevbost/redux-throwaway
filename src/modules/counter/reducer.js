import * as types from './actionTypes'

const initialState = {
  count: 0,
  isIncrememnting: false,
  isDecrementing: false,
  isEventing: false
}

export default ( state = initialState, action ) => {
  switch ( action.type ) {
  case types.INCREMENT_REQUESTED:
    return {
      ...state,
      isIncrememnting: true
    }

  case types.INCREMENT:
    return {
      ...state,
      count: state.count + 1,
      isIncrememnting: !state.isIncrememnting
    }

  case types.DECREMENT_REQUESTED:
    return {
      ...state,
      isDecrementing: true
    }

  case types.DECREMENT:
    return {
      ...state,
      count: state.count - 1,
      isDecrementing: !state.isDecrementing
    }

  default:
    return state
  }
}
