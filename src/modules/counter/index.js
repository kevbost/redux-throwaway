import React from 'react'
import { push } from 'react-router-redux'
import { bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import { Button, ButtonGroup, Row, Col } from 'react-bootstrap'
import {
  increment,
  incrementAsync,
  decrement,
  decrementAsync
} from '../../modules/counter/actions'

const Counter = props => {
  return (
    <Row className="counter">
      <Col xs={12} className="title">
        <h1>Counter</h1>
      </Col>
      <div className="body">
        <Col xs={12} md={6} className="text-center">
          <h3>Count:</h3>
          <div className="count">{props.count}</div>
          <div className="count">{props.event}</div>
        </Col>
        <Col xs={12} md={6}>
          <ButtonGroup justified>
            <Button href="#" onClick={props.increment} disabled={props.isIncrementing}>Increment</Button>
            <Button href="#" onClick={props.incrementAsync} disabled={props.isIncrementing}>Increment Async</Button>
          </ButtonGroup>
          <ButtonGroup justified>
            <Button href="#" onClick={props.decrement} disabled={props.isDecrementing}>Decrement</Button>
            <Button href="#" onClick={props.decrementAsync} disabled={props.isDecrementing}>Decrement Async</Button>
          </ButtonGroup>
          <ButtonGroup justified>
            <Button componentClass={Link} href="/about-us" to="/about-us">Go to About page via redux</Button>
          </ButtonGroup>
          <ButtonGroup justified>
            <Button componentClass={Link} href="/gvl-tonight" to="/gvl-tonight">Go to GVLTonight page via redux</Button>
          </ButtonGroup>
        </Col>
      </div>
    </Row>
  )
}

const mapStateToProps = state => {
  return {
    count: state.counter.count,
    isIncrementing: state.counter.isIncrememnting,
    isDecrementing: state.counter.isDecrementing,
    event: state.counter.events
  }
}

const mapDispatchToProps = dispatch => {
  return bindActionCreators({
    increment,
    incrementAsync,
    decrement,
    decrementAsync,
    changePage: () => { return push( '/about-us' ) }
  }, dispatch )
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)( Counter )
