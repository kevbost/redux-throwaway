import React from 'react'
import { captureStatus, getLoadingStatus } from 'redux-thunk-status'
import { submitRequest } from './actions'
import { connect } from 'react-redux'

class Viewer extends React.Component {

  render() {
    const { responseData, isLoading } = this.props

    const EventItem = ( event ) => {

      return (
        <div className="event">
          <div className="title">{event.event.title}</div>
          <div className="description">{event.event.description}</div>
          <hr />
        </div>
      )
    }

    return (
      <div>
        {responseData == undefined ? '' : responseData.map( event => {
          return (
            <EventItem key={event._id} event={event} />
          )
        })}
        {/* <div>{'hiiee'}</div>
        {/* <div>{responseData}</div> */}
        <div>{isLoading}</div>
      </div>
    )
  }
}

const mapStateToProps = state => {
  return {
    responseData: state.api.responseData,
    isLoading: getLoadingStatus( 'submit_request' )( state )
  }
}

const mapDispatchToProps = dispatch => {
  return {
    send_request: ( cty, timespan, presort ) => {
      dispatch( captureStatus( 'submit_request', submitRequest( cty, timespan, presort ) ) )
    }
  }
}

export default connect( mapStateToProps, mapDispatchToProps )( Viewer )
