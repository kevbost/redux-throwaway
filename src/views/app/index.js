import React from 'react'
import { BrowserRouter, Route, Link } from 'react-router-dom'
import Counter from '../../modules/counter'
import ApiContainer from '../../modules/api'
import Viewer from '../../modules/viewer'
import AppNav from '../../components/AppNav'
import { Navbar, Nav, NavItem, NavLink } from 'react-bootstrap'

const App = () => {
  return (
    <div>
      <AppNav />

      <main className="container">
        <Route exact path="/" component={ApiContainer} />
        <Route exact path="/counter" component={Counter} />
        <Route exact path="/viewer" component={Viewer} />
      </main>
    </div>
  )
}

export default App