import { combineReducers } from 'redux'
import { routerReducer } from 'react-router-redux'
import { reducer as asyncReducer } from 'redux-thunk-status'
import counter from './modules/counter/reducer'
import api from './modules/api/reducer'

const rootReducer = ( state, action ) => {
  return appReducer( state, action )
}

const appReducer = combineReducers({
  api,
  counter,
  async: asyncReducer,
  routing: routerReducer
})

export default rootReducer